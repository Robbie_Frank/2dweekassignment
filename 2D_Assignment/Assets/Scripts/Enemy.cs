﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int damage;
    public float moveSpeed;
    public GameObject player;
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Animator animator;
    Rigidbody2D rb2d;

    private void Start()
    {
        player = GameObject.Find("Player");
        rb2d = gameObject.GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        float moveDirection;
        if (player.transform.position.x < transform.position.x)
        {
            moveDirection = -1;
        }else
        {
            moveDirection = 1;
        }
        rb2d.velocity = new Vector2(moveDirection * moveSpeed, rb2d.velocity.y);
        if (rb2d.velocity.x >= 0)
        {
            spriteRenderer.flipX = false;
        }
        else
        {
            spriteRenderer.flipX = true;
        }
    }
}