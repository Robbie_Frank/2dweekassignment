﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minecart : MonoBehaviour
{
    public GameObject treasureGems;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            SpawnTreasure();
        }
    }

    void SpawnTreasure()
    {
        GameObject clone;
        clone = Instantiate(treasureGems, new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), transform.rotation);
        clone.GetComponent<Rigidbody2D>().AddForce(Vector3.up * 5f, ForceMode2D.Impulse);
        Destroy(gameObject);
    }
}