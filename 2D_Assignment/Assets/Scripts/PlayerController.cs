﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerController : MonoBehaviour
{
    // Properties
    [SerializeField] float runSpeed;
    [SerializeField] float jumpForce;
    public int money;
    int health = 100;


    // References
    Rigidbody2D rb2d;
    [SerializeField] Grid grid;
    [SerializeField] Tilemap tilemap;
    [SerializeField] TileBase tile;
    [SerializeField] TextMeshProUGUI moneyDisplay;
    [SerializeField] HealthBar healthBar;
    [SerializeField] SpriteRenderer spriteRenderer;
    [SerializeField] Animator animator;
    [SerializeField] TextMeshProUGUI gameOverText;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Ground");

            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int position = grid.WorldToCell(mousePos);
            float distance = Vector2.Distance(transform.position, mousePos);
            if (distance <= 1.75f)
            {
                MineTile(position);
            }
        }
        if (Input.GetButtonDown("Fire2"))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int position = grid.WorldToCell(mousePos);
            float distance = Vector2.Distance(transform.position, mousePos);
            if (!tilemap.HasTile(position) && distance <= 1.75f)
            {
                PlaceTile(position);
            }
        }
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(horizontalInput * runSpeed, rb2d.velocity.y);
        if (rb2d.velocity.x >= 0)
        {
            spriteRenderer.flipX = false;
        }
        else
        {
            spriteRenderer.flipX = true;
        }
        if (Mathf.Abs(horizontalInput) > 0)
        {
            animator.SetBool("isRunning", true);
        }
        else
        {
            animator.SetBool("isRunning", false);
        }
    }

    void Jump()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
    }

    void MineTile(Vector3Int position)
    {
        tilemap.SetTile(position, null);
    }

    void PlaceTile(Vector3Int position)
    {
        tilemap.SetTile(position, tile);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Currency")
        {
            money += collision.gameObject.GetComponent<Currency>().value;
            moneyDisplay.text = money.ToString();
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.tag == "Enemy")
        {
            if (collision.gameObject.transform.position.x > transform.position.x)
            {
                rb2d.AddForce(new Vector2(-3000f, 300f));
            }
            else
            {
                rb2d.AddForce(new Vector2(3000f, 300f));
            }
            int damageTaken = collision.gameObject.GetComponent<Enemy>().damage;
            TakeDamage(damageTaken);
        }
    }

    void TakeDamage(int damageAmount)
    {
        health -= damageAmount;
        healthBar.ModifyHealth(health);
        if (health <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        spriteRenderer.enabled = false;
        gameOverText.gameObject.SetActive(true);
        Time.timeScale = 0;
    }
}